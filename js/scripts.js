// Empty JS for your own code to be here
var SERVER_ROOT = "";
$(function() {
  // Handler for .ready() called.
  // load the item from the API
  $.get( SERVER_ROOT + "/api/themes/current", function( data ) {
	  createObjects(data['name'], data['objectNames'])
  });

  $('#submit-image').click(function() {
  	let image_id = $('.active.carousel-item').data('imageId');
  	let message = $('#comment').val();
  	let data = {"text": message, "objectId": image_id};

  	$.ajax({
	    headers: { 
	        'Accept': 'application/json',
	        'Content-Type': 'application/json'
	    },
	    'type': 'POST',
	    'url': SERVER_ROOT + '/api/messages',
	    'data': JSON.stringify(data),
	    'dataType': 'json'
    }).done(function() {
    	$('#comment').val(''); // reset the text area to empty string
    });
    return false;
  });

});

function createObjects(themeName, elements) {
	updateSlides(themeName, elements);
	updateIndicators(elements)
} 

function updateIndicators(elements) {
	var newHTML = "";
	if (elements.length < 8) {
		newHTML = '<ol class="carousel-indicators">'
		for (var i = 0; i < elements.length; i++) {
			newHTML += '<li data-slide-to="' + i +'" data-target="#carousel-528311"' + (i == 0 ? 'class="active"':'') + '>';
		}
		newHTML += '</ol>';

	} 

	$('.carousel-indicators').replaceWith(newHTML);
}


function updateSlides(themeName, elements) {
	var newHTML = '<div class="carousel-inner">';
	for (var i = 0; i < elements.length; i++) {
		let fileName = elements[i];
		newHTML += '<div data-image-id="' + i + '" class="carousel-item' + (i == 0 ? ' active ' : '') + '"><img class="d-block w-100" src="img/themes/' + themeName + '/' + fileName + '.png"></div>';

	}
	newHTML += '</div>'
	$('.carousel-inner').replaceWith(newHTML);
}
				